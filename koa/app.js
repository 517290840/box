const koa = require('koa');
const app = new koa();
const router = require('koa-router')();
const PORT = 7890;
const list = require('./mock/list.json')


router.get('/', async ctx => {
    ctx.body = {
        code: 1, 
        msg: "Hello, world!"
    };
});

router.get('/search', async ctx => {
    const { value } = ctx.query;
    const data = list.filter(item=>item.title.includes(value))
    ctx.body = {
        code: 1, 
        msg: "Hello, world!",
        data
    };
});


app.use(router.routes());

app.listen(PORT, () => console.log('服务启动成功，端口：' + PORT));