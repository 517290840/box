const Mock = require('mockjs');
const fs = require('fs');

const data = Mock.mock({
    "list|155": [
        {
            "num|1-99": 1,
            "price|1-999.2": 1,
            desc: "@cword(5, 10)",
            title: "@ctitle",
            img: "@img(100, 150, @color)"
        }
    ]
});



fs.writeFileSync("./list.json", JSON.stringify(data.list));

// num="2"
//   price="2.00"
//   desc="描述信息"
//   title="商品标题"
//   thumb="{{ imageURL }}"