const HttpAxios = ({url, method, data})=>{
  const baseUrl = "http://yapi.smart-xwork.cn/mock/109266";
  wx.showLoading({
    title: '加载中',
  })
  return new Promise((resolve, reject)=>{
    wx.request({
      url: baseUrl+url,
      method: method||"get",
      data,
      success(res){
        if(res.statusCode == 200){
          wx.hideLoading();
          resolve(res.data)
        } else if(res.statusCode == 401) {
          wx.showToast({
            title: '身份验证失败',
            duration: 2000
          })
        }
      },
      fail(err){
        wx.showToast({
          title: '请求失败',
          duration: 2000
        })
        reject(err);
      }
    })
  });
}

export default HttpAxios;