import HttpAxios from '../utils/http';

export const getSearch = (data)=>{
  return HttpAxios({
    url: "/list",
    data
  });
}
export const getmess = (data)=>{
  return HttpAxios({
    url: "/message",
    data
  });
}