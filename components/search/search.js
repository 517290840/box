const {
  debounce
} = require('../../utils/util');
import Toast from '@vant/weapp/toast/toast';
import { getSearch } from '../../api/search';

// components/search/search.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    disable: {
      type: Boolean,
      value: false
    },
    text: {
      type: String,
      value: "地图"
    },
    iconShow: {
      type: Boolean,
      value: false,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    value: '',
  },

  /**
   * 组件的方法列表
   */
  methods: {
    goSearch: function (e) {
      console.log(this.properties);
      if (this.properties.text === '地图') {
        wx.navigateTo({
          url: '/pages/search/search',
        })
      }
    },
    searchChange: debounce(async function (e) {
      console.log(this.data.value);
      const {data} = getSearch(this.data.value);
      console.log(data);
    }, 500)
  },
})